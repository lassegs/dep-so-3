<link rel="stylesheet" href="glitch.css">


<!-- .slide: data-background="https://oulu.com/ictoulu/wp-content/uploads/2023/12/digital-lock-image.png" data-background-size="cover" -->


<h1>DIGITAL SO3: <span class="glitch">Cybersikkerhet</span></h1>

---

<div class="thirdsplit">	
                <div class="gridleft">
                <h2>Om meg</h2>
                <ul>
                        <li>Lasse Gullvåg Sætre</li>
                        <li>Muliggjørende teknologier</li>
                        <li><a mailto:"lgs@forskningsradet.no">lgs@forskningsradet.no</a></li>
                    </ul>
                </div>
                <div class="gridright">
                    <img src="https://www.forskningsradet.no/siteassets/illustrasjoner/portefoljemuliggjorende-teknologier.svg">
                </div>
            </div>


---

<!-- .slide: data-background="https://www.socialenterprisebsr.net/wp-content/uploads/2018/12/Capture-1.jpg" data-background-size="30%" data-background-opacity="0.4" data-background-position="top right" -->

## Vastaamos datalekkasje

- Pasientdatabasen
- Dobbel utpressing: 450 000 Euro / 200 Euro
- 30 000 ofre


---

<!-- .slide: data-background-color="#003399" -->


## EU har våknet

<ul style="color: #ffcc00;">
<li>Fundamentalt for det digitale tiåret</li>
<li>Cybersikkerhetspakka (NIS2, CRA, CER, ++)</li>
<li>Det europeiske kompetansenteret for cybersikkerhet (ECCC)</li>
</ul>

---

![](https://upload.wikimedia.org/wikipedia/commons/6/6d/ENISA_logo.png)

- Overordnet ansvar for å holde EU cybersikkert
- Kunnskapsdeling, kapasitetsbygging, kompetanseheving

---

<!-- .slide: data-background="https://cybersecurity-centre.europa.eu/sites/default/files/styles/oe_theme_full_width/public/2023-05/new%20ECCC%205%20Visuals.jpg?itok=vPOnNaTp" data-background-size="contain" data-background-opacity="1" data-background-position="top" -->



<h1 style="color:white;">ECCC</h1>

<style> 
.reveal blockquote {
    background-color:rgb(250, 250, 250, .5);
}
</style>

> Article 4
>
> Objectives of the Competence Centre
>
> 1.   The Competence Centre shall have the **overall objective of promoting research, innovation and deployment in the area of cybersecurity** in order to fulfil the mission as set out in Article 3.

</div>

---

![](https://i.imgur.com/QFkTfgX.png)

---

## NCC-NO

"Main goal is to <span style="color:red;">increase participation in cybersecurity projects</span>, <span style="color:blue;">enhance cybersecurity knowledge and expertise in Norway</span>, <span style="color:green;">encourage usage of the projects results and services</span>, and <span style="color:violet;">involve relevant parties in the activities of the ECCC, Network and Community.</span>"

---

![](https://i.imgur.com/K8EivxQ.png)

---



- Nasjonale utlysninger
- Nasjonale NCC-utlysninger
- HEU og DEP-utlysninger

---

<!-- .slide: data-background="https://www.socialenterprisebsr.net/wp-content/uploads/2018/12/Capture-1.jpg" data-background-size="30%" data-background-opacity="0.4" data-background-position="top right" -->

## Hva skjedde med Vastaamo?

- 2021: Konkurs
- 2023: Mistenkte arrestert i Frankrike
- 2024: <span class="fragment glitch">???</span>

---

<!-- .slide: data-background-color="black" data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_INTROPOSTER_LONG_NAMETAG_2022-05-05-074407_pwrq.mp4" data-background-opacity="1"-->

## Spørsmål? 

- Lasse Gullvåg Sætre ([lgs@rcn.no](mailto:lgs@rcn.no))
- Kristoffer Vold Ulvestad ([kristoffer.vold.ulvestad@digdir.no](mailto:kristoffer.vold.ulvestad@digdir.no))
- [digitaleuropa@digdir.no](mailto:digitaleuropa@digdir.no)
